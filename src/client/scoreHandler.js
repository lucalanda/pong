const { THEME, MESSAGES } = require('../commons/constants');

const { PLAYER_1_SCORE_UPDATE, PLAYER_2_SCORE_UPDATE } = MESSAGES;
const { PRIMARY_COLOR_STR } = THEME;

const scoreStyle = {
    fill: PRIMARY_COLOR_STR,
    fontFamily: 'sans-serif',
    fontSize: 40
};

const scoreHandler = (game, socket) => {
    const scoreSound = game.sound.add('cheerful');
    const player1Score = game.add.text(300, 50, 0, scoreStyle).setOrigin(0.5, 0.5);
    const player2Score = game.add.text(500, 50, 0, scoreStyle).setOrigin(0.5, 0.5);

    socket.on(PLAYER_1_SCORE_UPDATE, (score) => {
        player1Score.text = score;
        scoreSound.play();
    });

    socket.on(PLAYER_2_SCORE_UPDATE, (score) => {
        player2Score.text = score;
        scoreSound.play();
    });
}

module.exports = scoreHandler;
