const gModal = require('./lib/gModal.min');

const alert = (message, callback) => {
    const modal = new gModal({
        title: message,
        buttons: [
            {
                content: 'OK',
                classes: "gmodal-button-red",
                bindKey: 13,
                callback
            }
        ],
        close: {
            closable: false,
        }
    });

    modal.show();
};

module.exports = alert;
