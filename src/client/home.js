const alert = require('./alert');
const GAMES_LIST_ID = 'games-list';
const NEW_GAME_INPUT_ID = 'new-game-name';

const VALID_GAME_REGEX = /^(\w|\ )+$/;

let availableGames = [];

const startHome = () => {
    refreshGames();
    setInterval(refreshGames, 1000);
};

const createGame = () => {
    const newGameNameInput = document.getElementById(NEW_GAME_INPUT_ID);
    const gameName = newGameNameInput.value;

    if (gameName.match(VALID_GAME_REGEX)) {
        window.location.href = getGameLocation(gameName);
    } else if (gameName == '') {
        alert('Insert a name for the new game.');
    } else {
        alert('Only letters, numbers and spaces are allowed.');
    }
}

const onInputKeyPress = ({ which, keyCode }) => {
    if (which != 13 && keyCode != 13) return true;
    createGame();
}

const refreshGames = () => {
    fetch('/available_games')
        .then(response => response.json())
        .then(games => {
            if (games == availableGames) return;
            availableGames = games;

            setGamesList(games);
        });
}

const setGamesList = (games) => {
    const gamesList = document.getElementById(GAMES_LIST_ID);

    while (gamesList.firstChild) gamesList.removeChild(gamesList.firstChild);

    games.forEach(game => {
        const li = document.createElement('li');
        const a = document.createElement('a');

        li.appendChild(a);
        a.text = game;
        a.href = getGameLocation(game);
        gamesList.appendChild(li);
    });

    showNoGamesPlaceholder(games.length === 0);
}

const showNoGamesPlaceholder = (show) => {
    const placeholder = document.getElementById('no-game-available');
    if (show) placeholder.classList = [];
    else placeholder.classList = ['hidden'];
}

const getGameLocation = game => '/game/' + game;

module.exports = { startHome, createGame, onInputKeyPress };
