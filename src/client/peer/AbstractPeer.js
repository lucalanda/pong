const SimpleSignalClient = require('simple-signal-client');

class AbstractPeer {
    constructor(socket, gameId) {
        if(this.constructor === 'AbstractPeer') {
            throw 'Cannot instantiate AbstractPeer';
        }

        this.signalClient = new SimpleSignalClient(socket);
        this.gameId = gameId;
    }

    onPeerData(data) {
        throw 'onPeerData not implemented!';
    }

    async connect() {
        return new Promise((resolve, reject) => {
            this.signalClient.on('discover', async ({ opponentSocketId }) => {
                if (!opponentSocketId) return;
    
                const { peer } = await this.signalClient.connect(opponentSocketId);
                peer.on('data', this.onPeerData.bind(this));
    
                this.peer = peer;

                resolve(true);
            });
    
            this.signalClient.on('request', async (request) => {
                const { peer } = await request.accept();
                this.peer = peer;
    
                peer.on('data', this.onPeerData.bind(this));
                
                resolve(true);
            });
    
            this.signalClient.discover(this.gameId);
        });
    }
    
}

module.exports = AbstractPeer;
