const AbstractPeer = require('./AbstractPeer');
const { MESSAGE_SEPARATOR } = require('../../commons/constants');

class ClientPeer extends AbstractPeer {
    constructor(socket, gameId, onGameUpdate) {
        super(socket, gameId);
        this.onGameUpdate = onGameUpdate;
    }

    sendPlayerMovement(newPaddleY) {
        const data = newPaddleY.toString();
        this.peer.send(data);
    }

    onPeerData(data) {
        const [paddleYStr, ballXStr, ballYStr] = data.toString().split(MESSAGE_SEPARATOR);
        this.onGameUpdate(parseFloat(paddleYStr), parseFloat(ballXStr), parseFloat(ballYStr));
    }
}

module.exports = ClientPeer;
