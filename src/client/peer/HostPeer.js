const AbstractPeer = require('./AbstractPeer');
const { MESSAGE_SEPARATOR } = require('../../commons/constants');

class HostPeer extends AbstractPeer {
    constructor(socket, gameId, onOpponentMovement) {
        super(socket, gameId);
        this.onOpponentMovement = onOpponentMovement;
    }

    sendGameUpdate(paddleY, ballX, ballY) {
        const data = [paddleY, ballX, ballY].join(MESSAGE_SEPARATOR);
        this.peer.send(data);
    }

    onPeerData(data) {
        const opponentY = parseFloat(data);
        this.onOpponentMovement(opponentY);
    }    
}

module.exports = HostPeer;
