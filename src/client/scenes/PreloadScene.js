class PreloadScene extends Phaser.Scene {
    constructor() {
        super('preload');
    }

    preload() {
        this.load.image('paddle1', '/assets/paddle1_rotated.png');
        this.load.image('paddle2', '/assets/paddle2_rotated.png');
        this.load.image('ball', '/assets/ball.png');
        this.load.audio('bounce', '/assets/sounds/boing.wav');
        this.load.audio('pop', '/assets/sounds/pop.wav');
        this.load.audio('squeak', '/assets/sounds/squeak.wav');
        this.load.audio('fart', '/assets/sounds/fart.wav');
        this.load.audio('cheerful', '/assets/sounds/cheerful.wav');
    }

    create() {
        const sceneName =
            window.playerType === 'host' ? 'hostGame' : 'clientGame';

        this.scene.start(sceneName);
    }
}

module.exports = PreloadScene;
