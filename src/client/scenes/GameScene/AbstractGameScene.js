const io = require('socket.io-client');
const scoreHandler = require('../../scoreHandler');
const alert = require('../../alert');
const {
    PLAYER_VELOCITY,
    MESSAGES,
    THEME,
} = require('../../../commons/constants');
const {
    OPPONENT_DISCONNECTED,
    PLAY_BOUNCE_SOUND,
    SHRINK_PADDLES_WIDTH,
    RESET_PADDLES_WIDTH
} = MESSAGES;
const { PRIMARY_COLOR_STR, PRIMARY_COLOR_NUM } = THEME;

const waitingTextStyle = {
    fill: PRIMARY_COLOR_STR,
    fontFamily: 'sans-serif',
    padding: 15,
    borderRadius: 10
};

const playerDistanceFromBorder = 40;

class AbstractGameScene extends Phaser.Scene {
    constructor(...args) {
        super(...args);
        if (this.constructor === AbstractGameScene) {
            throw new Error('Cannot instantiate abstract class');
        }
    }

    init() {
        this.isHost = window.playerType === 'host';
        this.gameName = window.gameName;
        this.bounceSound = this.sound.add('bounce');
        this.popSound = this.sound.add('pop');
        this.fartSound = this.sound.add('fart');
        this.cheerfulSound = this.sound.add('cheerful');
    }

    create() {
        const { width, height } = this.sys.game.canvas;

        this.physics.world.setBoundsCollision(false, false, true, true);

        this.player1 = this.createPlayer(playerDistanceFromBorder, height / 2);
        this.player2 = this.createPlayer(width - playerDistanceFromBorder, height / 2);

        this.cameras.main.setBackgroundColor('#fef1f1')

        this.setWaitingGameState();

        this.cursors = this.input.keyboard.createCursorKeys();

        this.socket = this.connectToServer();

        this.setupConnectionWithOpponent(this.socket)
            .then(() => this.startGame());

        scoreHandler(this, this.socket);
    }

    update() {
        if (!this.gameRunning) return;

        if (this.cursors.up.isDown) {
            this.playerPaddle.body.setVelocityY(-PLAYER_VELOCITY);
        } else if (this.cursors.down.isDown) {
            this.playerPaddle.body.setVelocityY(PLAYER_VELOCITY);
        } else {
            this.playerPaddle.body.setVelocityY(0);
        }
    }

    connectToServer() {
        const socket = io();

        const updatePaddlesWidth = (width) => {
            this.playerPaddle.displayHeight = width;
            this.opponentPaddle.displayHeight = width;
        };

        socket.on(PLAY_BOUNCE_SOUND, () => this.bounceSound.play());
        socket.on(SHRINK_PADDLES_WIDTH, (width) => {
            this.fartSound.play();
            updatePaddlesWidth(width);
        });
        socket.on(RESET_PADDLES_WIDTH, (width) => {
            if (width != this.playerPaddle.displayHeight) this.popSound.play();
            updatePaddlesWidth(width);
        });
        socket.on(OPPONENT_DISCONNECTED, () => {
            alert('Opponent disconnected, returning to home...', () => window.location.href = '/');
        });

        return socket;
    }

    startGame() {
        this.waitingText.destroy();
        this.gameRunning = true;

        this.setupBall();
    }

    setupBall(x = 400) {
        this.ball = this.physics.add.image(x, 300, 'ball')
            .setDisplaySize(15, 15)
            .setTintFill(PRIMARY_COLOR_NUM);

    }

    createPlayer(x, y, image = 'paddle1') {
        return this.physics.add.image(x, y, image)
            .setDisplaySize(20, 100)
            .setTintFill(PRIMARY_COLOR_NUM)
            .setCollideWorldBounds(true);
    }

    setWaitingGameState() {
        this.waitingText = this.add.text(400, 300, 'Waiting for the other player...', waitingTextStyle).setOrigin(0.5, 0.5);
        this.gameRunning = false;
    }

    setPreviousPaddleY() {
        this.playerPaddle.previousY = null;
    }

    async setupConnectionWithOpponent() {
        throw new Error('Not implemented!');
    }
}

module.exports = AbstractGameScene;
