const AbstractGameScene = require('./AbstractGameScene');
const HostPeer = require('../../peer/HostPeer');
const { fixVelocity } = require('../../util');
const { BALL_SPEED, MESSAGES } = require('../../../commons/constants');
const {
    PLAYER_1_SCORED,
    PLAYER_2_SCORED,
    BALL_BOUNCE,
} = MESSAGES;

// TODO refactor into constants.js
const playerDistanceFromBorder = 40;

class HostGameScene extends AbstractGameScene {
    constructor() {
        super('hostGame');
    }

    create() {
        super.create();
        this.playerPaddle = this.player1;
        this.opponentPaddle = this.player2;

        this.setPreviousPaddleY();

        this.playerPaddle.setImmovable(true);
        this.opponentPaddle.setImmovable(true);
    }

    update() {
        super.update();
        if(!this.gameRunning) return;

        const newPaddleY = this.playerPaddle.y;

        const { x, y } = this.ball;

        if (x > 800 || x < 0) this.resetBall();

        this.peer.sendGameUpdate(newPaddleY, x, y);

        this.ball.previousPosition = { x, y };
        this.playerPaddle.previousY = newPaddleY;
    }

    setupBall(x, xDirection = Math.random() > 0.5 ? 1 : -1) {
        super.setupBall(x);

        const yDirection = Math.random() > 0.5 ? 1 : -1;
        this.ball.setVelocity(BALL_SPEED * xDirection, Math.random() * 20 * yDirection).setBounce(1, 1);
        this.ball.setCollideWorldBounds(true);
        this.ball.previousPosition = { x: null, y: null };

        this.physics.add.collider(this.playerPaddle, this.ball, this.onPaddleBallCollision, null, this);
        this.physics.add.collider(this.opponentPaddle, this.ball, this.onPaddleBallCollision, null, this);
    }

    resetBall() {
        const { x } = this.ball;
        const message = x > 800 ? PLAYER_1_SCORED : PLAYER_2_SCORED;
        this.socket.emit(message);
        this.ball.destroy();

        let newX, xDirection;
        // host scored
        if (x > 800) {
            newX = 800 - playerDistanceFromBorder * 2.5;
            xDirection = -1;
        } else { // client scored
            newX = playerDistanceFromBorder * 2.5;
            xDirection = 1;
        }

        this.setupBall(newX, xDirection);
    }

    async setupConnectionWithOpponent(socket) {
        this.peer = new HostPeer(socket, this.gameName, y => this.opponentPaddle.y = y);

        await this.peer.connect();
    }

    onPaddleBallCollision(paddle, ball) {
        const yDifference = paddle.y - ball.y;
        ball.setVelocityY(-5 * yDifference);
        fixVelocity(ball, BALL_SPEED);

        this.socket.emit(BALL_BOUNCE);
    }
};

module.exports = HostGameScene;
