const AbstractGameScene = require('./AbstractGameScene');
const ClientPeer = require('../../peer/ClientPeer');

class ClientGameScene extends AbstractGameScene {
    constructor() {
        super('clientGame');
    }
    
    create() {
        super.create();
        this.playerPaddle = this.player2;
        this.opponentPaddle = this.player1;

        this.setPreviousPaddleY();
    }

    update() {
        super.update();
        if(!this.gameRunning) return;
        
        const newPaddleY = this.playerPaddle.y;

        if (this.playerPaddle.previousY != newPaddleY) {
            this.peer.sendPlayerMovement(newPaddleY);
        }
        this.playerPaddle.previousY = newPaddleY;
    }

    async setupConnectionWithOpponent(socket) {
        this.peer = new ClientPeer(socket, this.gameName, (opponentY, ballX, ballY) => {
            this.opponentPaddle.y = opponentY;
            this.ball.setPosition(ballX, ballY);
        });

        await this.peer.connect();
    }
};

module.exports = ClientGameScene;
