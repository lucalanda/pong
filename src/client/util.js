const fixVelocity = (sprite, desiredVelocity) => {
    var body = sprite.body
    var angle, vx, vy;
    vx = body.velocity.x;
    vy = body.velocity.y;

    angle = Math.atan2(vy, vx);
    vx = Math.cos(angle) * desiredVelocity;
    vy = Math.sin(angle) * desiredVelocity;
    body.setVelocity(vx, vy);
}

const constraintVelocity = (sprite, maxVelocity) => {
    var body = sprite.body
    var angle, currVelocitySqr, vx, vy;
    vx = body.velocity.x;
    vy = body.velocity.y;
    currVelocitySqr = vx * vx + vy * vy;
    if (currVelocitySqr > maxVelocity * maxVelocity) {
        angle = Math.atan2(vy, vx);
        vx = Math.cos(angle) * maxVelocity;
        vy = Math.sin(angle) * maxVelocity;
        body.setVelocity(vx, vy);
    }
};

module.exports = { fixVelocity, constraintVelocity };
