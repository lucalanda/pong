/** @type {import("../../typings/phaser")} */

const PreloadScene = require('./scenes/PreloadScene.js');
const HostGameScene = require('./scenes/GameScene/HostGameScene.js');
const ClientGameScene = require('./scenes/GameScene/ClientGameScene.js');

const { startHome, createGame, onInputKeyPress } = require('./home');

let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    parent: 'game',
    physics: {
        default: 'arcade',
    },
    scene: [PreloadScene, HostGameScene, ClientGameScene],
};


const startGame = (gameName, playerType) => {
    // TODO passare attraverso gameConfig
    window.gameName = gameName;
    window.playerType = playerType;
    new Phaser.Game(config);
}

window.startGame = startGame;
window.onInputKeyPress = onInputKeyPress;
window.startHome = startHome;
window.createGame = createGame;
