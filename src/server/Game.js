const {
    PLAYER_1_SCORE_UPDATE,
    PLAYER_2_SCORE_UPDATE,
    PLAYER_1_SCORED,
    PLAYER_2_SCORED,
    OPPONENT_DISCONNECTED,
    BALL_BOUNCE,
    PLAY_BOUNCE_SOUND,
    SHRINK_PADDLES_WIDTH,
    RESET_PADDLES_WIDTH
} = require('../commons/constants').MESSAGES;

const initalPaddlesLength = 100;
const bouncesLimitForPaddleShortening = 3;
const paddleShorteningPercentage = 0.1;


class Game {
    constructor(hostSocket, clientSocket) {
        this.hostSocket = hostSocket;
        this.clientSocket = clientSocket;
        this.paddlesLength = initalPaddlesLength;
        this.bouncesCount = 0;

        this.scores = { host: 0, client: 0 };

        this.setPlayerScoreUpdate();
        this.setBallBounceUpdate();
        this.setupOnOpponentDisconnectedUpdate();
    }

    setPlayerScoreUpdate() {
        this.hostSocket.on(PLAYER_1_SCORED, () => {
            this.scores.host++;
            this.bouncesCount = 0;
            this.resetPaddlesLength();
            this.emit(PLAYER_1_SCORE_UPDATE, this.scores.host);
        });

        this.hostSocket.on(PLAYER_2_SCORED, () => {
            this.scores.client++;
            this.bouncesCount = 0;
            this.resetPaddlesLength();
            this.emit(PLAYER_2_SCORE_UPDATE, this.scores.client);
        });
    }

    setBallBounceUpdate() {
        this.hostSocket.on(BALL_BOUNCE, () => {
            this.emit(PLAY_BOUNCE_SOUND);
            this.bouncesCount++;
            
            if (this.bouncesCount == bouncesLimitForPaddleShortening) {
                this.bouncesCount = 0;
                this.shrinkPaddlesLength();
            }
        });
    }

    setupOnOpponentDisconnectedUpdate() {
        this.hostSocket.on('disconnect', () => this.clientSocket.emit(OPPONENT_DISCONNECTED));
        this.clientSocket.on('disconnect', () => this.hostSocket.emit(OPPONENT_DISCONNECTED));
    }

    shrinkPaddlesLength() {
        this.paddlesLength *= (1 - paddleShorteningPercentage);
        this.emit(SHRINK_PADDLES_WIDTH, this.paddlesLength);
    }

    resetPaddlesLength() {
        this.paddlesLength = initalPaddlesLength;
        this.emit(RESET_PADDLES_WIDTH, this.paddlesLength);
    }

    emit(...args) {
        this.hostSocket.emit(...args);
        this.clientSocket.emit(...args);
    }
}

module.exports = Game;
