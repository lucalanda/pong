const path = require('path');
const express = require('express');
const ejs = require('ejs');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io').listen(server);
const SimpleSignalServer = require('simple-signal-server');
const Game = require('./Game');

const PORT = process.env.PORT || 3000;
const rootFolder = path.resolve(__dirname + '/../..');

let pendingGames = {};

const getRequestedGameName = (url) => {
    const urlWithoutParams = url.replace(/\?.*/, '');
    const gameName = urlWithoutParams.split('/game/')[1];

    return gameName;
}

const setupWebServer = () => {
    app.engine('html', ejs.renderFile);

    app.use(express.static(rootFolder + '/public/'));

    app.get('/', (_req, res) => {
        res.sendFile(rootFolder + '/home.html');
    });

    app.get('/available_games', (_req, res) => {
        res.send(Object.keys(pendingGames));
    });

    app.get('/game/:gameName', (req, res) => {
        const gameName = getRequestedGameName(req.url);
        const playerType = pendingGames[gameName] ? 'client' : 'host';

        res.render(rootFolder + '/game.html', { game: gameName, playerType });
    });

    server.listen(PORT, () => {
        console.log(`Listening on ${server.address().port}`);
    });
};

const setupMultiplayerServer = () => {
    const signalServer = SimpleSignalServer(io);

    signalServer.on('discover', (request) => {
        const { socket, discoveryData } = request;
        const socketId = socket.id;

        const gameId = discoveryData;
        let opponentSocketId = null;

        let pendingGame = pendingGames[gameId];
        if (pendingGame) {
            const { hostSocket, hostSocketId } = pendingGame;
            opponentSocketId = hostSocketId;
            const clientSocket = socket;
            delete pendingGames[gameId];

            new Game(hostSocket, clientSocket);
        } else {
            pendingGames[gameId] = { hostSocket: socket, hostSocketId: socketId };
        }

        request.discover({ opponentSocketId });
    });

    signalServer.on('request', (request) => request.forward());
};

setupMultiplayerServer();
setupWebServer();