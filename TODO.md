# TODO
* posizionamento paddles => DONE
  * abilitazione movimento paddle 1
  * controllo momentaneo player2 tramite w-s
* introduzione pallina
  * sparata a random a sinistra o destra => DONE
  * rimbalza sui paddles => DONE
  * quando esce dal campo viene rimessa in gioco => DONE
* rimbalzo pallina ottimale sui paddles => DONE

* entrata in multiplayer
  * scene iniziale con bottoni "join as host" e "join as client" => DONE
  * premendoli si entra nel gioco (senza pallina) => DONE
    * si vedono entrambi i paddle => DONE
    * si può muovere solo il proprio => DONE
  * sincronizzazione paddles => DONE
    * host e client, finchè non inizia il gioco => DONE
      * visualizzano il messaggio d'attesa => DONE
      * non possono muovere il proprio paddle => DONE
    * host e client si scambiano la posizione dei propri paddle nell'update => DONE
  * movimento pallina => DONE
    * l'host spawna la pallina => DONE
    * ad ogni update, l'host manda {x,y} della pallina insieme alla propria {y} => DONE
    * client ad ogni aggiornamento della pallina la aggiorna => DONE
    * refactor: anche il client invia l'aggiornamento come stringa => DONE
* giocabilità minima => DONE
  * fix rimbalzo pallina tipo breakout => DONE
  * respawn pallina appena esce dal campo => DONE
* gestione migliore creazione/join partita => DONE
  * la home mostra lista di partite attive => DONE
  * c'è un miniform per creare una partita sopra alla lista => DONE
  * la lista di partite attive si aggiorna automaticamente => DONE
  * la partita è identificabile come "http://.../game/nomepartita" => DONE
  * creare una partita porta all'indirizzo "http://.../game/nomepartita" => DONE
* passaggio a p2p => DONE
  * usare oggetti Peer per inviare i movimenti al posto del socket normale => DONE
* accesso effettivo alla nuova partita => DONE
  * accedere all'indirizzo di una nuova partita la crea => DONE
  * accedere all'indirizzo di una partita esistente crea il collegamento con l'altro giocatore => DONE
  * la pagina game ha un link che riporta alla home => DONE
  * fix server: riceve sempre gameId 'abc' eppure funziona, c'è del codice da togliere... => DONE
* grafica: il gioco usa i colori del tema rosa, rosso e bianco => DONE
  * sfondo rosa => DONE
  * paddle del colore primario rosso => DONE
  * game: il link Home è dato dal logo => DONE
* punteggio => DONE
  * visualizzazione punteggio giocatori => DONE
  * il punteggio cambia quando la pallina esce dal campo => DONE
* gestione disconnessione giocatori => DONE
* ghirigori => DONE
  * sfondo => DONE
  * suoni al rimbalzo e allo score => DONE
  * respawn palla migliore => DONE

* i paddles si restringono
  * ogni 4 bounce del 10% => DONE
  * suono quando si restringono => DONE
  * tornano della lunghezza originale dopo uno score => DONE
  * l'angolo di rimalzo della palla tiene conto della lunghezza del paddle => DONE
  * idea: il paddle si muove più veloce tanto è più piccolo?

* i giocatori possono mettere pausa
  * sempre gestito dal server?

* più regole che si alternano tra uno score e l'altro
  * regola 1: i paddles si restringono ogni 3/4 bounce
  * regola 2: la pallina diventa più veloce ogni 3/4 bounce
  * regola 3: compare una nuova pallina ogni 4/5 bounce
  * regola 4: i comandi si invertono ogni 2 bounce

* fix
  * client mostra placeholder in caso non ci siano giochi disponibili => DONE
  * text input nome partita non ha contorno nero quando selezionato in chrome => DONE
  * url partita gestiti dal server
    * al tasto invio sull'input si crea la partita => DONE
    * la creazione avviene tramite post /create che redirige a /game/nomepartita (ed è il server a ridare eventuali errori)
    * il server restituisce alla fetch gli url oltre che i nomi delle partite, così che il client li setti direttamente
    * il controllo regex nome partita è eseguito lato server
  * cancellazione partita quando entrambi gli host spariscono
    * o è in uno stato inconsistente
  * nome partita mostrato url decoded
* fix grafica 2
  * viene mostrato un link per la condivisione sotto al gioco
  * il logo per tornare alla home è mostrato in maniera più decente in alto a sinistra
* fix artefatti grafici
  * bordi strani gioco su firefox
* risoluzione
  * fix visualizzazione su schermi 1366x768
  * ridurre a 640 * 480, ridurre paddles e pallina e zoommare il gioco
  * valutare fullscreen mantenendo risoluzione HD?
* gioco da telefono/tablet
  * resize dinamico https://gamedevacademy.org/creating-mobile-games-with-phaser-3-and-cordova/#Scaling_the_Game
  * supportare i click sullo schermo come comandi

# Refactor
* fare reject della promessa di connessione nel peer in caso di errore

----------------------- APPUNTI -----------------------------------


# logiche più complesse, da valutare
* es. il paddle si restringe nel tempo
* es. la pallina diventa più veloce nel tempo
* es. compaiono più palline
  * es. quando la pallina colpisce un ostacolo si sdoppia
* es. compaiono oggetti o ostacoli mobili su cui la pallina rimbalza
* es. compaiono percorsi in cui deve passare la pallina
* es. trappole fanno invertire i comandi
* es. compaiono omini che camminano e se li colpisci con la palla hai svantaggi
  * l'omino si incazza, ti spara e il tuo paddle diventa più piccolo
  * l'omino fa "ahia" e la palla rimbalza semplicemente indietro
* es. compaiono ostacoli e ogni volta che la pallina li tocca cambia dimensione (più piccola o più grande)

# idee multiplayer
## Host calcola tutto
* creare un player host e un player client
* i calcoli li fa tutti l'host
  * al movimento del paddle, manda la posizione al client
  * ad ogni frame manda la posizione della pallina al client
* il client 
  * ad ogni movimento manda all'host la posizione del suo paddle
  * riceve posizioni paddle host e pallina e ci aggiorna il suo gioco

## Host calcola quasi tutto
* i due giocatori si scambiano posizione paddles
* l'host calcola a ogni evento (rimbalzo, spawn) posizione e velocità pallina, e la comunica al client
* client applica posizione e velocity quando li riceve

## Ibrido
* scambio posizioni paddles in tempo reale
* ogni 0.2 secondi l'host invia al client attuale posizione e velocity della pallina
  * il pacchetto include un timestamp in millisecondi
  * il client verifica la distanza della pallina e applica velocity e distanza se
    * il timestamp ricevuto è abbastanza recente
    * la pallina è abbastanza lontana

## Ognuno calcola i suoi impatti e li manda all'altro
* il server calcola un istante preciso in cui posizionare la pallina, una posizione e una velocità
* i client scambiano i dati sulla posizione dei loro paddles e impostano correttamente l'altro
* 